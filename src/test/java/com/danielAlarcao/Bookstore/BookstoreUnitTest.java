package com.danielAlarcao.Bookstore;

import com.danielAlarcao.Bookstore.Basket.BasketService;
import com.danielAlarcao.Bookstore.Book.BookService;
import com.danielAlarcao.Bookstore.Stock.StockService;
import com.danielAlarcao.Bookstore.Store.Store;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest
public class BookstoreUnitTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    BasketService basketService;
    @MockBean
    BookService bookService;
    @MockBean
    StockService stockService;
    @MockBean
    Store store;

    @Test
    public void listall() throws Exception {

        when(bookService.getAllBooks()).thenReturn(Collections.emptyList());

        MvcResult r = mockMvc.perform(MockMvcRequestBuilders.get("/listAll")).andReturn();

        System.out.println(r.getResponse());

        verify(bookService).getAllBooks();
    }

}
