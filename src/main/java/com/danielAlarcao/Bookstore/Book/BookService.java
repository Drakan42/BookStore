package com.danielAlarcao.Bookstore.Book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository booksRepository;

    public List<Book> getAllBooks() {
        return new ArrayList<>(booksRepository.findAll());
    }

    public boolean bookexists(Book book) {
        return booksRepository.existsById(book.getId());
    }

    public List<Book> searchTitle(String searchString) {
        return booksRepository.findByTitleContaining(searchString);
    }

    public List<Book> searchAuthor(String searchString) {
        return booksRepository.findByAuthorContaining(searchString);
    }

    public Book save(Book book) {
        return booksRepository.save(book);
    }

    public Optional<Book> getBook(int bookid) {
        return booksRepository.findById(bookid);
    }
}
