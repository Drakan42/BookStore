package com.danielAlarcao.Bookstore;

import com.danielAlarcao.Bookstore.Book.Book;
import com.danielAlarcao.Bookstore.Book.BookRepository;
import com.danielAlarcao.Bookstore.Stock.Stock;
import com.danielAlarcao.Bookstore.Stock.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class DataLoader {
    static boolean loaded = false;

    private BookRepository bookRepository;
    private StockRepository stockRepository;

    @Autowired
    public DataLoader(BookRepository bookRepository, StockRepository stockRepository) {
        this.bookRepository = bookRepository;
        this.stockRepository = stockRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    private boolean createDB() {
        boolean result = false;
        try {
            URL url = new URL("https://raw.githubusercontent.com/contribe/contribe/dev/bookstoredata/bookstoredata.txt");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                String[] split = inputLine.split(";");
                Book save = bookRepository.save(new Book(split[0], split[1], split[2]));
                stockRepository.save(new Stock(save, Integer.parseInt(split[3])));
            }
            in.close();
            result = true;
        } catch (Exception e) {
            System.out.println(e);
            result = false;
        }

        DataLoader.loaded = result;

        return result;
    }

    public void run() {
        if (createDB()) System.out.println("############# DATA IMPORTED ##################");
    }
}