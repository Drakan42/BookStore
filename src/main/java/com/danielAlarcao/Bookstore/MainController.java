package com.danielAlarcao.Bookstore;

import com.danielAlarcao.Bookstore.Basket.Basket;
import com.danielAlarcao.Bookstore.Basket.BasketService;
import com.danielAlarcao.Bookstore.Book.Book;
import com.danielAlarcao.Bookstore.Book.BookService;
import com.danielAlarcao.Bookstore.Stock.StockService;
import com.danielAlarcao.Bookstore.Store.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class MainController {

    private BookService booksService;
    private Store store;
    private BasketService basketService;
    private StockService stockService;

    @Autowired
    public MainController(BookService bookService, Store store, BasketService basketService, StockService stockService) {
        this.basketService = basketService;
        this.booksService = bookService;
        this.store = store;
        this.stockService = stockService;
    }

    @RequestMapping("/")
    public String sayHello() {
        return "Welcome to the Book Store";
    }

    @RequestMapping("/list")
    @ResponseBody()
    public Book[] list(@RequestParam(value = "searchString", required = true) String searchString, Model model) {
        return store.list(searchString);
    }

    @RequestMapping("/listAll")
    @ResponseBody
    public List<Book> list() {
        return booksService.getAllBooks();
    }

    @RequestMapping("/addQuantity")
    @ResponseBody
    public boolean addQuantity(@RequestParam(value = "bookId", required = true) int bookId, @RequestParam(value = "quantity", required = true) int quantity) {
        return booksService.getBook(bookId).map(book -> store.add(book, quantity))
                .orElse(false);
    }

    @RequestMapping("/addBook")
    @ResponseBody
    public Book addBook(@RequestParam(value = "title", required = true) String title, @RequestParam(value = "author", required = true) String author, @RequestParam(value = "price", required = true) BigDecimal price) {
        return booksService.save(new Book(title, author, price));
    }

    @RequestMapping("/setQuatity")
    @ResponseBody
    public boolean setQuantity(@RequestParam(value = "bookId", required = true) int bookId, @RequestParam(value = "quantity", required = true) int quantity) {
        return booksService.getBook(bookId).map(book -> store.setQuantity(book, quantity))
                .orElse(false);
    }

    @RequestMapping(value = "/buy")
    @ResponseBody
    public int[] buy(@RequestParam(value = "basketId", required = true) Integer basketId) throws Exception {
        return basketService.findById(basketId).map(Basket::getbooks).map(books -> store.buy(books))
                .orElseThrow(() -> new Exception("Could Not buy Basket Please make sure the Basket Exist"));
    }

    @RequestMapping("/newBasket")
    @ResponseBody
    public Basket newBasket() {
        return basketService.createBasket();
    }

    @RequestMapping("/showBasket")
    @ResponseBody
    public Basket showBasket(@RequestParam(value = "basketId", required = true) int basketId) throws Exception {
        return basketService.findById(basketId)
                .orElseThrow(() -> new Exception("Could Not Display Basket Please make sure the Basket Exist"));
    }

    @RequestMapping("/addBookToBasket")
    @ResponseBody
    public Basket addBookToBasket(@RequestParam(value = "basketId", required = true) int basketId, @RequestParam(value = "bookId", required = true) int bookId) throws Exception {
        return booksService.getBook(bookId).map(book -> basketService.addBook(basketId, book))
                .orElseThrow(() -> new Exception("Could Not add Book To Basket Please make sure the Basket and Book Exist"));
    }

    @RequestMapping("/removeBookFromBasket")
    @ResponseBody
    public Basket removeBookFromBasket(@RequestParam(value = "basketId", required = true) int basketId, @RequestParam(value = "bookId", required = true) int bookId) throws Exception {
        return booksService.getBook(bookId).map(book -> basketService.removeBook(basketId, book))
                .orElseThrow(() -> new Exception("Could Not remove Book To Basket Please make sure the Basket and Book Exist"));
    }

}
