package com.danielAlarcao.Bookstore.Basket;

import com.danielAlarcao.Bookstore.Book.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class BasketService {

    @Autowired
    private BasketRepository basketRepository;

    public Optional<Basket> findById(int id) {
        return basketRepository.findById(id);
    }

    public Basket createBasket() {
        return save(new Basket());
    }

    public Basket save(Basket basket) {
        Arrays.asList(basket.getbooks()).forEach(book -> basket.setTotalPrice(book.getPrice().add(basket.getTotalPrice())));
        return basketRepository.saveAndFlush(basket);
    }

    public Basket addBook(int basketId, Book book) {
        return basketRepository.findById(basketId).map(basket -> basket.addBook(book)).map(basket -> save(basket)).orElse(new Basket());
    }

    public Basket removeBook(int basketId, Book book) {
        return basketRepository.findById(basketId).map(basket -> basket.removeBook(book)).orElse(new Basket());
    }

}
