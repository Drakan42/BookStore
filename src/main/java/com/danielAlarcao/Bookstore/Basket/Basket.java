package com.danielAlarcao.Bookstore.Basket;

import com.danielAlarcao.Bookstore.Book.Book;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Basket")
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private BigDecimal totalPrice;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "basket_books",
            joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "basket_id", referencedColumnName = "id")
    )
    private List<Book> books = new ArrayList<>();

    public Basket() {
        this.totalPrice = BigDecimal.ZERO;
    }

    public Basket(List<Book> books) {
        this.books = books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Book[] getbooks() {
        return books.toArray(new Book[books.size()]);
    }

    public int getBasketId() {
        return id;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public Basket setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public Basket addBook(Book book) {
        List<Book> result = new ArrayList<>();
        if (!this.books.isEmpty()) {
            result.addAll(books);
            result.add(book);
        } else
            result.add(book);
        this.books = result;

        this.books.stream().map(Book::getPrice).map(price -> this.setTotalPrice(this.totalPrice.add(price)));
        return this;
    }

    public Basket removeBook(Book book) {
        List<Book> result = new ArrayList<>();
        result.addAll(books);
        result.remove(result.indexOf(book));
        this.books = result;
        return this;
    }
}
