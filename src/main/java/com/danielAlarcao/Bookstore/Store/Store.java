package com.danielAlarcao.Bookstore.Store;

import com.danielAlarcao.Bookstore.Book.Book;
import com.danielAlarcao.Bookstore.Book.BookService;
import com.danielAlarcao.Bookstore.Stock.Stock;
import com.danielAlarcao.Bookstore.Stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class Store implements IBookList {

    @Autowired
    private BookService bookService;
    @Autowired
    private StockService stockService;

    @Override
    public Book[] list(String searchString) {
        List<Book> result = new ArrayList<>();
        result.addAll(bookService.searchTitle(searchString));
        result.addAll(bookService.searchTitle(searchString));
        return result.stream().distinct().toArray(Book[]::new);
    }

    @Override
    public boolean add(Book book, int quantity) {
        try {
            Stock stockToUpdate = stockService.getStockFromBook(book);
            stockToUpdate.setAmount(stockToUpdate.getAmount() + quantity);
            stockService.save(stockToUpdate);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean setQuantity(Book book, int quantity) {
        try {
            Stock stockToUpdate = stockService.getStockFromBook(book);
            stockToUpdate.setAmount(quantity);
            stockService.save(stockToUpdate);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int[] buy(Book... books) {
        return Arrays.stream(books).map(book -> {
            if (!bookService.bookexists(book)) {
                return 2;
            } else if (stockService.getStockFromBook(book).getAmount() <= 0) {
                return 1;
            } else
                return 0;
        }).mapToInt(i -> i).toArray();

    }

}
