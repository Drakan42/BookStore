package com.danielAlarcao.Bookstore.Store;

import com.danielAlarcao.Bookstore.Book.Book;

public interface IBookList {
    public Book[] list(String searchString);

    public boolean add(Book book, int quantity);

    public int[] buy(Book... books);

}
