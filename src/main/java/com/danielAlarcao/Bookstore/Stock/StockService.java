package com.danielAlarcao.Bookstore.Stock;

import com.danielAlarcao.Bookstore.Book.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;

    public List<Stock> getAllBooks() {
        List<Stock> books = new ArrayList<>();
        stockRepository.findAll().forEach(books::add);
        return books;
    }

    public Stock getStock(Stock stock) {
        return stockRepository.getOne(stock.getStockId());
    }

    public Stock getStockFromBook(Book book) {
        return stockRepository.findByBookId(book.getId());
    }

    public Stock save(Stock stock) {
        return stockRepository.save(stock);
    }

}
