package com.danielAlarcao.Bookstore;

import com.danielAlarcao.Bookstore.Basket.Basket;
import com.danielAlarcao.Bookstore.Basket.BasketService;
import com.danielAlarcao.Bookstore.Book.Book;
import com.danielAlarcao.Bookstore.Book.BookService;
import com.danielAlarcao.Bookstore.Stock.StockService;
import com.danielAlarcao.Bookstore.Store.Store;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.math.BigDecimal;

@ShellComponent
public class BookstoreCommands {

    private final BasketService basketService;
    private final BookService booksService;
    private final Store store;
    private final StockService stockService;
    private DataLoader dataLoader;
    private ObjectMapper jsonMaper;

    @Autowired
    public BookstoreCommands(BookService bookService, Store store, BasketService basketService, StockService stockService, DataLoader dataLoader) {
        this.basketService = basketService;
        this.booksService = bookService;
        this.store = store;
        this.stockService = stockService;
        this.dataLoader = dataLoader;
        dataLoader.run();
        this.jsonMaper = new ObjectMapper();
        this.jsonMaper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    @ShellMethod("hello")
    public String hello() {
        return "hello aaaa";
    }


    @ShellMethod("List all Books")
    public String listAll() throws JsonProcessingException {
        String result = this.jsonMaper.writeValueAsString(booksService.getAllBooks());
        return result;
    }

    @ShellMethod("List a Books")
    public String list(String searchString) throws JsonProcessingException {
        String result = this.jsonMaper.writeValueAsString(store.list(searchString));
        return result;
    }

    @ShellMethod("add Quantity to a book")
    public String addQuantity(int bookId, int quantity) throws JsonProcessingException {
        String result = this.jsonMaper.writeValueAsString(booksService.getBook(bookId).map(book -> store.add(book, quantity)).orElse(false));
        return result;
    }


    @ShellMethod("ADD a Book to the Store")
    public String addBook(String title, String author, BigDecimal price) throws JsonProcessingException {
        String result = this.jsonMaper.writeValueAsString(booksService.save(new Book(title, author, price)));
        return result;
    }

    @ShellMethod("Set the total Quantity of a book")
    public String setQuantity(int bookId, int quantity) throws JsonProcessingException {
        String result = this.jsonMaper.writeValueAsString(booksService.getBook(bookId).map(book -> store.setQuantity(book, quantity))
                .orElse(false));
        return result;
    }

    @ShellMethod("buy a basket")
    public String buy(Integer basketId) throws Exception {
        String result = this.jsonMaper.writeValueAsString(basketService.findById(basketId).map(Basket::getbooks).map(books -> store.buy(books))
                .orElseThrow(() -> new Exception("Could Not buy Basket Please make sure the Basket Exist")));
        return result;
    }

    @ShellMethod("create a new basket")
    public String newBasket() throws JsonProcessingException {
        return this.jsonMaper.writeValueAsString(basketService.createBasket());
    }

    @ShellMethod("Show the Contents of a basket")
    public String showBasket(int basketId) throws Exception {
        String result = this.jsonMaper.writeValueAsString(basketService.findById(basketId)
                .orElseThrow(() -> new Exception("Could Not Display Basket Please make sure the Basket Exist")));
        return result;
    }

    @ShellMethod("add a book to a basket")
    public String addBookToBasket(int basketId, int bookId) throws Exception {
        String result = this.jsonMaper.writeValueAsString(booksService.getBook(bookId).map(book -> basketService.addBook(basketId, book))
                .orElseThrow(() -> new Exception("Could Not add Book To Basket Please make sure the Basket and Book Exist")));
        return result;
    }

    @ShellMethod("remove a book from a basket")
    public String removeBookFromBasket(int basketId, int bookId) throws Exception {
        String result = this.jsonMaper.writeValueAsString(booksService.getBook(bookId).map(book -> basketService.removeBook(basketId, book))
                .orElseThrow(() -> new Exception("Could Not remove Book To Basket Please make sure the Basket and Book Exist")));
        return result;
    }

}
